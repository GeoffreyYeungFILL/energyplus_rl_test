import gym
from gym import spaces
from gym.utils import seeding

import os
import opyplus as op

import numpy as np

# ========== Energy Plus Stuff here ==========


def _schedule_edit(schedule, inner):
    if isinstance(inner[0], tuple) or isinstance(inner[0], list):
        for item in inner:
            schedule.add_fields(
                "Until: " + str(item[0]) + ',' + str(item[1]))
    else:
        assert 24 % len(inner) == 0
        for i in range(len(inner)):
            schedule.add_fields(
                "Until: " + str((24 // len(inner)) * (i + 1)) + ":00")
            schedule.add_fields(str(inner[i]))
    return schedule


class EpmHandler:
    def __init__(self, source_epm_path, epw_path):
        self.epm = op.Epm.from_idf(source_epm_path)
        self.epw = epw_path

    def simulate(self, name='test-simulation'):
        self.epm.save('tmp.idf')
        return op.simulate('tmp.idf', self.epw, name)

    def create_schedule(self, name, params):
        schedule = handler.epm.Schedule_Compact.add(
            name=name, schedule_type_limits_name="Any Number")
        if (isinstance(params, list)) or (isinstance(params, tuple)):
            schedule.add_fields(
                "Through: 12/31",
                "For: AllDays")
            schedule = _schedule_edit(schedule, params)
        elif isinstance(params, dict):
            schedule.add_fields(
                "Through: 12/31",
            )
            for key in params:
                schedule.add_fields("For: " + key)
                schedule = _schedule_edit(schedule, params[key])
        return schedule


eplus_dir_path = op.get_eplus_base_dir_path((9, 4, 0))
epw_path = os.path.join(
    "./hk_weather",
    "CHN_Hong.Kong.SAR.450070_CityUHK.epw"
)
handler = EpmHandler("Exercise1D-Solution-2.idf", epw_path)


def get_current_row(df, time):
    return df[(df['hour'] == time[0]) & (df['day'] ==
                                         time[1]) & (df['month'] == time[2])]

# ========== RL Stuff here ==========


class HvacSim(gym.Env):
    def __init__(self):
        self.seed()
        self.reset()
        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Box(
            low=np.array([0, 0]),
            high=np.array([1, 23]),
            dtype=np.int16
        )
        self.time = np.array([0, 1, 1])  # time is [hour, day, month]
        # we can determine if it's weekday by looking at energy plus output
        #   (it tells you if it's Monday, Tuesday etc)
        # but on step(action), we'll have to change either weekday_schedule or weekend_schedule
        # so we need to cache is_weekday
        self.is_weekday = False
        self.schedule_id = 0
        self.weekday_schedule = [0.0] * 24
        self.weekend_schedule = [0.0] * 24

        handler.create_schedule('init schedule', {
            "Weekdays": self.weekday_schedule, "Holidays AllOtherDays": self.weekend_schedule})
        handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].availability_schedule_name = 'init schedule'

    # ========== helper functions ==========

    def seed(self, seed=None):
        # ???
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _increment_time(self):
        self.time[0] += 1
        # hour, 0-index
        if self.time[0] >= 24:
            self.time[0] = 0
            self.time[1] += 1
        # day
        if (self.time[1] > 31 and self.time[2] in [1, 3, 5, 7, 8, 10, 12]) \
                or (self.time[1] > 30 and self.time[2] in [4, 6, 9, 11]) \
                or (self.time[1] > 28 and self.time[2] == 2):
            self.time[1] = 1
            self.time[2] += 1
        # month
        if self.time[2] > 12:
            self.time[2] = 0

    def _calculate_reward3(self, df):
        pmv_cost = 0
        for row in df['zone one people,Zone Thermal Comfort Pierce Model Effective Temperature PMV']:
            pmv_cost += row ** 2
        energy_cost = 0
        for row in df['zone one,Zone Air System Sensible Cooling Energy']:
            energy_cost += row
        for row in df['zone one,Zone Air System Sensible Heating Energy']:
            energy_cost += row
        energy_cost /= 10_000_000  # TODO: what should be the factor???
        print('energy cost:', energy_cost)
        return - pmv_cost - energy_cost

    # ========== main code ==========

    def step(self, action):
        # action changes whether we should turn on AC for the next hour
        if self.is_weekday:
            self.weekday_schedule[self.time[0]] = action
        else:
            self.weekend_schedule[self.time[0]] = action

        # TODO: don't create a new schedule every time, reuse old one
        new_schedule_name = f"test air schedule {self.schedule_id}"
        handler.create_schedule(new_schedule_name, {
            "Weekdays": self.weekday_schedule, "Holidays AllOtherDays": self.weekend_schedule})
        # print(handler.epm.SetpointManager_Scheduled[0])
        handler.epm.ZoneHVAC_IdealLoadsAirSystem[0].availability_schedule_name = new_schedule_name

        # run sim
        sim = handler.simulate('my-first-simulation')
        eso = sim.get_out_eso()
        df = eso.get_data()

        reward = self._calculate_reward3(df)

        day_type = get_current_row(df, self.time)['day_type'].iloc[0]
        self.is_weekday = day_type in [
            'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']

        self.schedule_id += 1
        self._increment_time()

        observation = [int(self.is_weekday), self.time[0]]
        done = False  # the simulation never finishes
        info = {}  # ...?

        return observation, reward, done, info

    def reset(self):
        # ???
        pass


# main code
env = HvacSim()
env.reset()
while True:
    print('time:', env.time)  # time is [hour, day, month]
    random_action = env.action_space.sample()
    print('action:', random_action)
    observation, reward, done, info = env.step(random_action)
    print('reward:', reward)
    print('=====')
env.close()
